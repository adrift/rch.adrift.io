# deploying custom clusterissuer resources

```bash
kubectl create -f https://gitlab.com/adrift/rch.adrift.io/raw/master/issuer/letsencrypt-prod-issuer.yml
kubectl create -f https://gitlab.com/adrift/rch.adrift.io/raw/master/issuer/letsencrypt-staging-issuer.yml
```