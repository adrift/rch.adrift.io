apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: whoami-adrift-io
  annotations:
    kubernetes.io/ingress.class: nginx
    certmanager.k8s.io/cluster-issuer: letsencrypt-staging
    kubernetes.io/tls-acme: "true"
spec:
  rules:
  - host: whoami.adrift.io
    http:
      paths:
      - path: /
        backend:
          serviceName: whoami
          servicePort: 80
  tls:
  - secretName: whoami-adrift-io
    hosts:
    - whoami.adrift.io